<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/**
 * @var Dingo\Api\Routing\Router
 */
$api = app('Dingo\Api\Routing\Router');

$api->version('v1', ['middleware' => 'cors'], function ($api) {
    $api->group(['prefix' => 'v1'], function ($api) {

        $api->group(['prefix' => 'companies'], function ($api) {
            $api->post('', '\App\Http\Controllers\CompaniesController@store');


            $api->group(['prefix' => 'types'], function ($api) {

                $api->post('', '\App\Http\Controllers\CompanyTypesController@store');

            });

        });







    });
});